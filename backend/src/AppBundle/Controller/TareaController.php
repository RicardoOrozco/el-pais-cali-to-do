<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tarea;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\Query;
use AppBundle\Entity\Categoria;
use \AppBundle\Entity\Usuario;

/**
 * Tarea controller.
 *
 * @Route("tarea")
 */
class TareaController extends Controller {

    /**
     * Lists all tarea entities.
     *
     * @Route("/", name="tarea_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $tareas = $em->getRepository(Tarea::class)->createQueryBuilder('t')->getQuery()->getResult(Query::HYDRATE_ARRAY);
        return new JsonResponse($tareas);
    }

    /**
     * Creates a new tarea entity.
     *
     * @Route("/new", name="tarea_new")
     * @Method({"POST"})
     */
    public function newAction(Request $request) {

        $data = json_decode($request->getContent());

        if (!empty($data)) {
            $categoria = $this->getDoctrine()->getRepository(Categoria::class)->find($data->categoria_id);
            $usuarioCreacion = $this->getDoctrine()->getRepository(Usuario::class)->find(1);

            $tarea = new Tarea();
            $tarea->setCategoria($categoria);
            $tarea->setTarea($data->tarea);
            $tarea->setFechaCreacion(new \DateTime());
            $tarea->setUsuarioCreacion($usuarioCreacion);

            $em = $this->getDoctrine()->getManager();
            $em->persist($tarea);
            $em->flush();
        }
        return new JsonResponse($data);
    }

    /**
     * Finds and displays a tarea entity.
     *
     * @Route("/{id}", name="tarea_show")
     * @Method("GET")
     */
    public function showAction(Tarea $tarea) {
        $deleteForm = $this->createDeleteForm($tarea);

        return $this->render('tarea/show.html.twig', array(
                    'tarea' => $tarea,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tarea entity.
     *
     * @Route("/{id}/edit", name="tarea_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Tarea $tarea) {
        $deleteForm = $this->createDeleteForm($tarea);
        $editForm = $this->createForm('AppBundle\Form\TareaType', $tarea);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tarea_edit', array('id' => $tarea->getId()));
        }

        return $this->render('tarea/edit.html.twig', array(
                    'tarea' => $tarea,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tarea entity.
     *
     * @Route("/{id}", name="tarea_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Tarea $tarea) {
        $form = $this->createDeleteForm($tarea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tarea);
            $em->flush();
        }

        return $this->redirectToRoute('tarea_index');
    }

    /**
     * Creates a form to delete a tarea entity.
     *
     * @param Tarea $tarea The tarea entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tarea $tarea) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('tarea_delete', array('id' => $tarea->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
