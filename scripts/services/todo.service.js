'use strict';

app.factory('TodoFactory', function($http) {

    var urlGetList = 'http://localhost/el-pais-cali-to-do/backend/web/tarea';
    var urlSetTask = 'http://localhost/el-pais-cali-to-do/backend/web/tarea/new';
    var urlGetCategories = 'http://localhost/el-pais-cali-to-do/backend/web/categoria';

    var TodoFactory = {
        getList: function() {
            return $http.get(urlGetList)
                .success(function(data) {
                    return data;
                })
                .error(function(err) {
                    console.log(err);
                });
        },
        setTask: function(categoria_id, tarea) {
            return $http.post(urlSetTask, { categoria_id: categoria_id, tarea: tarea })
                .success(function(data) {
                    return data;
                })
                .error(function(err) {
                    console.log(err);
                });
        },
        getCategories: function() {
            return $http.get(urlGetCategories)
                .success(function(data) {
                    return data;
                })
                .error(function(err) {
                    console.log(err);
                });
        }
    };

    return TodoFactory;
});