'use strict';

app.controller('TodoListController', function($scope, TodoFactory) {

    var todoList = this;
    todoList.todoText = '';
    todoList.selectedCategory = '';

    TodoFactory.getList().then(function(data) {

        todoList.todos = data.data;

        todoList.addTodo = function() {
            TodoFactory.setTask(todoList.selectedCategory, todoList.todoText).then(function(data) {
                todoList.todos.push({ tarea: todoList.todoText, estado: false });
                todoList.todoText = '';
            });

        };

        todoList.remaining = function() {
            var count = 0;
            angular.forEach(todoList.todos, function(todo) {
                count += todo.estado ? 0 : 1;
            });
            if (count != 1) {
                return count + ' tareas pendientes';
            } else {
                return count + ' tarea pendientes';
            }
        };
    });

    TodoFactory.getCategories().then(function(data) {
        todoList.categories = data.data;
    });
});