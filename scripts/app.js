'use strict';

var app = angular.module('TodoApp', [
    'ui.router'
]);

app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state(
            'home', {
                url: '/home',
                templateUrl: 'views/home.html',
                controller: 'TodoListController',
            }
        );

    $urlRouterProvider.otherwise('/home');
});