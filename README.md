# El Pa�s Cali To Do

Proyecto de prueba tecnica para ingresar a El Pa�s Cali de Ricardo Orozco Le�n

Demo:
https://empresacomercialvirtual.com/el-pais-cali-to-do

# Tecnologias

Backend:
Base de datos: MySql
Framework PHP: Symfony 3.4

Frontend:
Framework Javascript: AngularJS

# Instalaci�n
1. Clonar proyecto: ``` git clone https://RicardoOrozco@bitbucket.org/RicardoOrozco/el-pais-cali-to-do.git ```
2. Migrar la base de datos: /el-pais-to-do.sql
3. Acceder a la carpeta /backend e instalar dependencias: ``` composer install ``` (la base de datos se llama el-pais-to-do)
4. Actualizar las url de las APIs en el archivo /scripts/services/todo.service.js Lineas 5, 6 y 7, reemplazar http://localhost/ por la direcci�n publica de la carpeta del proyecto

# NOTA:
 - El proyecto esta pensado para que frontend y backend se encuentren en el mismo servidor, no tiene omisi�n de cors
 - Se utilizaron las tecnologias que se comento pose�n en el ambiente laboral (Symfony 3: Drupal y Prestashop - AngularJS)
 
# PD:
 - Aunque no se desea tomar como excusa, no se culmino el total del requerimiento ya que eran tecnologias desconocidas hasta el momento y solo pude dedicarle 1 d�a al desarrollo.

